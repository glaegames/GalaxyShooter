﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Image MainMenu;
    public Image livesImage;
    public Text scoreText;
    public Text highScoreText;
    public Sprite[] lives;
    public int score;
    public int highScore = 0;

    public void ShowTitleScreen()
    {
        MainMenu.enabled = true;
        highScoreText.enabled = true;
        scoreText.enabled = false;
        livesImage.enabled = false;
        scoreText.enabled = false;
        score = 0;

        if (highScore > 0)
        { 
            highScoreText.text = "High Score : " + highScore;
        }
    }

    public void HideTitleScreen()
    {
        MainMenu.enabled = false;
        highScoreText.enabled = false;
        scoreText.enabled = true;
        livesImage.enabled = true;
        scoreText.enabled = true;
        score = 0;
        scoreText.text = "Score : " + score;
    }

    public void UpdateLives(int currentLives)
    {
        livesImage.sprite = lives[currentLives];
    }

    public void UpdateScore(GameObject other)
    {
        if (other.tag.Equals("Enemy"))
        {
            score += 10;
        }
        else if (other.tag.Equals("Asteroid"))
        {
            score += 5;
        }

        if (score >= highScore)
        {
            highScore = score;
        }

        scoreText.text = "Score : " + score;
    }
}
