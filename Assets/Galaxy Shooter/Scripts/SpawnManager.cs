﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    [SerializeField]
    private GameObject _playerPrefab;
    [SerializeField]
    private GameObject _enemyShipPrefab;
    [SerializeField]
    private GameObject _asteroidPrefab;
    [SerializeField]
    private GameObject[] _powerups;
    [SerializeField]
    private float _enemySpawnTime = 2.0f;
    [SerializeField]
    private float _powerupSpawnTime = 5.0f;
    [SerializeField]
    private float _asteroidSpawnTime = 1.0f;

    private float _playerStartPosY = -4.0f;
    private float _xAxisLimit = 7.75f;
    private float _yAxisLimit = 6.5f;

    public void Begin()
    {
        Instantiate(_playerPrefab, new Vector3(0, _playerStartPosY, 0), Quaternion.identity);
        StartCoroutine(SpawnAsteroidRoutine());
        StartCoroutine(SpawnEnemyRoutine());
        StartCoroutine(SpawnPowerupRoutine());
    }

    public IEnumerator SpawnEnemyRoutine()
    {
        while(true)
        {
            Instantiate(_enemyShipPrefab, GetRandomSpawnPosition(), Quaternion.identity);
            yield return new WaitForSeconds(_enemySpawnTime);
        }
    }
    public IEnumerator SpawnAsteroidRoutine()
    {
        while (true)
        {
            Instantiate(_asteroidPrefab, GetRandomSpawnPosition(), Quaternion.identity);
            yield return new WaitForSeconds(_asteroidSpawnTime);
        }
    }


    public IEnumerator SpawnPowerupRoutine()
    {
        while (true)
        {
            Instantiate(_powerups[Random.Range(0, 3)], GetRandomSpawnPosition(), Quaternion.identity);
            yield return new WaitForSeconds(_powerupSpawnTime);
        }
    }

    private Vector3 GetRandomSpawnPosition()
    {
        return new Vector3(Random.Range(-_xAxisLimit, _xAxisLimit), _yAxisLimit, 0);
    }
}
