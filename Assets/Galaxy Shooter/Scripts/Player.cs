﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public bool tripleShotActive = false;
    public bool shieldActive = false;
    public int lives = 3;

    [SerializeField]
    private GameObject _laserPrefab;
    [SerializeField]
    private GameObject _tripleShotPrefab;
    [SerializeField]
    private GameObject _explosionPrefab;
    [SerializeField]
    private GameObject _shieldGameObject;
    [SerializeField]
    private float _speed = 7.5f;
    [SerializeField]
    private float _fireRate = 0.25f;
    [SerializeField]
    private float _tripleShotDuration = 4.0f;
    [SerializeField]
    private float _speedBoostDuration = 5.0f;
    [SerializeField]
    private float _speedBoostMultiplier = 1.5f;
    [SerializeField]
    private GameObject[] _engines;
    private float _nextFire = 0.0f;
    private int _hitCount = 0;
    private UIManager _uiManager;
    private GameManager _gameManager;
    private SpawnManager _spawnManager;
    private AudioSource _audioSource;

    // Use this for initialization
    private void Start()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _spawnManager = GameObject.Find("SpawnManager").GetComponent<SpawnManager>();
        _audioSource = GetComponent<AudioSource>();

        _hitCount = 0;

        if (_uiManager != null)
        {
            _uiManager.UpdateLives(lives);
        }
    }

    // Update is called once per frame
    private void Update()
    {
        Movement();

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }

    private void Movement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        float xAxisLimit = 9.5f;
        float yAxisLimit = 4.2f;

        transform.Translate(new Vector3(horizontalInput, verticalInput, 0) * _speed * Time.deltaTime);

        // Limit player movement and "wrap" on the X axis
        if (transform.position.x >= xAxisLimit)
        {
            transform.position = new Vector3(-xAxisLimit, transform.position.y, 0);
        }
        else if (transform.position.x <= -xAxisLimit)
        {
            transform.position = new Vector3(xAxisLimit, transform.position.y, 0);
        }

        // Limit player movement on the Y axis
        if (transform.position.y > 0)
        {
            transform.position = new Vector3(transform.position.x, 0, 0);
        }
        else if (transform.position.y < -yAxisLimit)
        {
            transform.position = new Vector3(transform.position.x, -yAxisLimit, 0);
        }
    }

    private void Shoot()
    {
        float gunPositionCentreY = 0.925f;

        if (Time.time > _nextFire)
        {
            _audioSource.Play();

            if (tripleShotActive)
            {
                Instantiate(_tripleShotPrefab, transform.position, Quaternion.identity);

            }
            else
            {
                Instantiate(_laserPrefab, transform.position + new Vector3(0, gunPositionCentreY, 0), Quaternion.identity);
            }
            
            _nextFire = Time.time + _fireRate;
        }
    }

    public void Damage()
    {
        if (shieldActive)
        {
            shieldActive = false;
            _shieldGameObject.SetActive(false);
            return;
        }

        _hitCount++;

        if (_hitCount == 1)
        {
            _engines[0].SetActive(true);
        }
        else if (_hitCount == 2)
        {
            _engines[1].SetActive(true);
        }
        

        lives--;
        _uiManager.UpdateLives(lives);

        if (lives < 1)
        {
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            _gameManager.gameOver = true;
            _spawnManager.StopAllCoroutines();
            _uiManager.ShowTitleScreen();
            Destroy(this.gameObject);
        }
    }

    public void ActivateTripleShot()
    {
        tripleShotActive = true;
        StartCoroutine(DeactivateTripleShot());
    }

    public IEnumerator DeactivateTripleShot()
    {
        yield return new WaitForSeconds(_tripleShotDuration);
        tripleShotActive = false;
    }

    public void ActivateSpeedBoost()
    {
        _speed *= _speedBoostMultiplier;
        StartCoroutine(DeactivateSpeedBoost());
    }

    public IEnumerator DeactivateSpeedBoost()
    {
        yield return new WaitForSeconds(_speedBoostDuration);
        _speed /= _speedBoostMultiplier;
    }

    public void ActivateShield()
    {
        shieldActive = true;
        _shieldGameObject.SetActive(true);
    }

}
