﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

    [SerializeField]
    private GameObject _explosionPrefab;
    [SerializeField]
    private AudioClip _audioClip;
    [SerializeField]
    private float _speed = 7.5f;
    private UIManager _uiManager;
    private float _yAxisLimit = 6.5f;

    // Use this for initialization
    void Start ()
    {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        // Limit movement and destroy object if it makes it past the player
        if (transform.position.y <= -_yAxisLimit)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag.Equals("Laser"))
        {
            if (other.transform.parent != null)
            {
                Destroy(other.transform.parent.gameObject);
            }
            Destroy(other.gameObject);
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(_audioClip, Camera.main.transform.position, 1f);
            _uiManager.UpdateScore(this.gameObject);
            Destroy(this.gameObject);
        }
        else if (other.tag.Equals("Player"))
        {
            Player player = other.GetComponent<Player>();

            if (player != null)
            {
                player.Damage();
            }

            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            AudioSource.PlayClipAtPoint(_audioClip, Camera.main.transform.position, 1f);
            Destroy(this.gameObject);
        }
    }
}
