# GalaxyShooter
A never-ending 2D space shooter game made with Unity, featuring enemies, asteroids, powerups and a score system.

![](GalaxyShooter.mp4)
